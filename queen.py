# A Python program to print all  
# permutations using library function 
from itertools import permutations 
import random

        
def best_so_far(potentials):
    seen = []
    for newsuitor in potentials:
        
        count_total = len(seen)
        count_less = sum(x < newsuitor for x in seen)
        count_greater = count_total - count_less

        min_pos = 1+count_less
        max_pos = 10-count_greater

        if (count_total >= 2):
            #code here
            expected = float(min_pos + max_pos)/2.0
            predicted = calc_avg(min_pos, max_pos)

            print(expected, predicted)
            if (expected > predicted):
                return newsuitor
        
            
        seen.append(newsuitor)
    return (newsuitor)

def pick_first(potentials):
    return potentials.pop()


def calc_avg(low, high):
    if(low == 10):
        return 10.0
    
    rg = high - low
    total_avg = 0
    for val in range(low, high+1):
        perm_size = val - low
        perms = permutations(range(1, val), perm_size)
        total_perms = 0
        total_sum = 0
        for p in perms:
            total_perms += 1
            lp = list(p)
            total_sum += sum(lp)
        total_sum += sum(range(val+1, 10+1)) * total_perms
        avg = float(total_sum)/float(total_perms)
        avg = avg/float(10.0 -low)
        #print("Low: ", low, "High: ", high, "Val: ", val, "PermSize: ", perm_size, "Perms: ", perms, "Total Sum: ", total_sum, "Total Perms: ", total_perms, "Avg: ", avg, "Range: ", 10.0 - low)
        total_avg += avg
    final = float(total_avg)/float(rg+1)
    return final

# Get all permutations of [1, 2, 3] 
perm = permutations([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) 
  
# Print the obtained permutations 
perm_count = 10*9*8*7*6*5*4*3*2*1
print('Total: ', perm_count)
total = 0
total_count = 0
for potential in list(perm):    
    
    if random.randint(1,10000) == 1:
        res = best_so_far(list(potential))
        total += res
        total_count += 1
        print (list(potential), res)
    
    
print (total/total_count)
