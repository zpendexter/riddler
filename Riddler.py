from itertools import permutations


def get_best_match(permutation):
    starting_point = 1
    
    return permutation[starting_point]


def main():
    men = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    perms = list(permutations(men))

    total = 0
    for perm in perms:
        total += get_best_match(perm)

    avg = float(total)/float(len(perms))
    print(avg)





main()